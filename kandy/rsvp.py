from google.appengine.ext import ndb


class Rsvp(ndb.Model):
    """A main model for representing an individual RSVP."""
    can_attend = ndb.BooleanProperty()
    guest_code = ndb.StringProperty()
    comment = ndb.StringProperty(indexed=False)
    dietary_requirements = ndb.StringProperty(indexed=False)
    guest_group = ndb.StringProperty(indexed=False, repeated=True)
    guest_count = ndb.IntegerProperty(indexed=False)
    not_guest_group = ndb.StringProperty(indexed=False, repeated=True)
    not_guest_count = ndb.IntegerProperty(indexed=False)
    song = ndb.StringProperty(indexed=False)
    song_ids = ndb.StringProperty(indexed=False, repeated=True)
    rsvp_date = ndb.DateTimeProperty(auto_now_add=True)
