guest_map = {
  'afam':          { 'guests': [ 'Teresa', 'Tom' ] },
  'giraffe':       { 'guests': [ 'Josie', 'Steve', 'Lily-Mae' ], 'kids': True },
  'jorsty':        { 'guests': [ 'Kirsty', 'Jordan', 'Isla', 'Theo' ], 'kids': True },
  'burnhill':      { 'guests': [ 'Pamela', 'William' ] },
  'nangan':        { 'guests': [ 'Pat', 'Ron' ] },
  'compton':       { 'guests': [ 'Glenn', 'Naglaa', 'Hannah', 'Natalie' ] },
  'portland':      { 'guests': [ 'Douglas', 'Marian', 'Stuart', 'Maya', 'Lily' ] },
  'scoobysnacks':  { 'guests': [ 'Sean', 'Julia' ] },
  'chestnut':      { 'guests': [ 'Martin', 'Zoki', 'Marco' ] },
  'castlecroft':   { 'guests': [ 'Carole', 'John' ] },
  'bombay':        { 'guests': [ 'Nick', 'Sam' ] },
  'teamcoke':      { 'guests': [ 'Sonja' ] },
  'klex':          { 'guests': [ 'Lex', 'Kirsten' ] },
  'kingsley':      { 'guests': [ 'Simon', 'Ellie' ] },
  'soupclub':      { 'guests': [ 'Richard', 'Andreina' ] },
  'clubofsoup':    { 'guests': [ 'James', 'Abi' ] },
  'soton':         { 'guests': [ 'Ema', 'Matt', 'Nia' ], 'kids': True },
  'briz':          { 'guests': [ 'Laurie' ] },
  'fryer':         { 'guests': [ 'Katie' ] },
  'sytch':         { 'guests': [ 'James', 'Hayley' ] },
  'panda':         { 'guests': [ 'Amanda' ] },
  'quay':          { 'guests': [ 'Juliette', 'Damian' ] },
  'thomogen':      { 'guests': [ 'Imogen', 'Thomas' ] },
  'kevlev':        { 'guests': [ 'Kevin' ] },
  'mrmrssmith':    { 'guests': [ 'Sarah', 'Michael', 'Reece', 'Olivia' ] },
  'clevers':       { 'guests': [ 'Simon', 'Clare', 'Millie', 'Felix', 'Lola' ], 'kids': True },
  'corrs':         { 'guests': [ 'Yvonne', 'Julian' ] },
  'gibson':        { 'guests': [ 'Stephen', 'Jasmine' ] },
  'brelly':        { 'guests': [ 'Kelly', 'Brad' ] },
  'legohouse':     { 'guests': [ 'Steve', 'Claire' ] },
  'theramin':      { 'guests': [ 'Nath', 'Penny' ] },
  'texmex':        { 'guests': [ 'Ellie', 'Eric', 'Baby Smith' ], 'kids': True },
  'jecky':         { 'guests': [ 'Becky', 'John' ] },
  'benson':        { 'guests': [ 'Ben', 'Jenny', 'Jenson', 'Cohen' ], 'kids': True },
  'plumbsims':     { 'guests': [ 'Stacey', 'Joe' ] },
  'steps':         { 'guests': [ 'Claire', 'Ben' ] },
  'kirbinator':    { 'guests': [ 'Paul' ] },
  'winelord':      { 'guests': [ 'Andrew' ] },
  'gamesnight':    { 'guests': [ 'Duncan', 'Anna' ] },
  'skatanic':      { 'guests': [ 'Debbie', 'Dan' ] },
  'devon':         { 'guests': [ 'Chris', 'Nikki', 'William' ], 'kids': True },
  'mandalore':     { 'guests': [ 'Martin', 'Peter' ] },
  'hammertime':    { 'guests': [ 'Toby' ] },
  'spanglish':     { 'guests': [ 'Pablo', 'Rosa', 'Teresa' ], 'kids': True },
  'magma':         { 'guests': [ 'Martin', 'Maggie' ] },
  'firebaked':     { 'guests': [ 'Ismael', 'Carmen' ] },
  'carbonara':     { 'guests': [ 'Sam' ] },
  'milgold':       { 'guests': [ 'Danny', 'Emma' ] },
  'arty':          { 'guests': [ 'Armin', 'Patty' ] },
  'hankyco':       { 'guests': [ 'Hannah', 'Mike' ] },
  'owltastic':     { 'guests': [ 'Neha' ] },
  'sparklehammer': { 'guests': [ 'Ola' ] },
  'struttyostuff': { 'guests': [ 'Mike' ] },
  'cooper':        { 'guests': [ 'Mark', 'Jane' ] },
  'orton':         { 'guests': [ 'Grant', 'Ruth' ] },
  'jolen':         { 'guests': [ 'John', 'Helen' ] },
  'waldron':       { 'guests': [ 'Mick', 'Kirsty' ] },
  'bushes':        { 'guests': [ 'David', 'Samantha' ] },
  'fillion':       { 'guests': [ 'Nick', 'Megan' ] },
}


def all_guests():
  return guest_map


def lookup_guests(guest_code):
  if (guest_code in guest_map):
    return guest_map[guest_code]
  return None


def filter_out_guests(guest_dict, rsvp_lists_to_remove):
  all_guests_dict = all_guests()
  filtered_guests = {}
  rsvp_codes_to_remove = []

  for r in rsvp_lists_to_remove:
    for rsvp in r:
      rsvp_codes_to_remove.append(rsvp.guest_code)

  for code in all_guests_dict:
    if not code in rsvp_codes_to_remove:
      filtered_guests[code] = all_guests_dict[code]['guests']

  return filtered_guests


def guest_count():
  count = 0
  for guest_code in guest_map:
    count += len(guest_map[guest_code])
  return count
