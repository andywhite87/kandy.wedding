import Vue from 'vue/dist/vue.min';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import store from '../store/store';
import NavIndicator from '../components/nav-indicator';
import Who from '../views/who';
import Where from '../views/where';
import When from '../views/when';
import Rsvp from '../views/rsvp';


const scrollEl = document.getElementById('app-wrapper')
const scrollBase = document.querySelector('h1').clientHeight * 1.1;
const scrollDelay = 350;

const views = [Who, Where, When, Rsvp];
const routePaths = [];
let routeLinks = '';


const Children = [];
views.forEach((view, i) => {
  let routeObj = {
    path: '/' + view.id,
    component: { template: view.template }
  };
  if (i === 0) {
    routeObj.alias = '';
  }

  Children.push(routeObj);

  routePaths.push('/' + view.id);

  routeLinks += `
    <li><router-link to="${view.id}" class="router-link">
      <span>${view.title}</span>
    </router-link></li>`;
});


const Parent = {
  data () {
    return {
      transitionName: 'slide-left'
    };
  },
  beforeRouteUpdate (to, from, next) {
    const toPos = routePaths.indexOf(to.path);
    const fromPos = routePaths.indexOf(from.path);
    let navigationAllowed = true;

    this.transitionName = toPos < fromPos ? 'slide-right' : 'slide-left';

    // Prevent navigation if form is dirty
    if (from.path === '/' + Rsvp.id && store.state.formDirty) {
      navigationAllowed = window.confirm(store.state.formDirtyText);
    }

    if (navigationAllowed) {
      store.commit('routeUpdate', { path: to.path });
      if (scrollEl.scrollTop > scrollBase * 1.5) {
        setTimeout(function() {
          scrollEl.scrollTop = scrollBase;
        }, scrollDelay);
      }
      next();
    }
    else {
      next(false);
    }
  },
  template: `<div>
    <transition :name="transitionName">
      <router-view class="child-view"></router-view>
    </transition>
  </div>`
};


const routeTemplate = Vue.component('route-template', {
  template: `
    <div>
      <div>
        <ul class="router-nav">
          ${routeLinks}
          <nav-indicator></nav-indicator>
        </ul>
        <span></span>
      </div>
      <transition name="fade" mode="out-in">
        <router-view class="parent-view"></router-view>
      </transition>
    </div>
  `
});


const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Parent,
      children: Children
    }
  ]
});

export {
  router,
  routeLinks,
  routePaths,
  routeTemplate
};
