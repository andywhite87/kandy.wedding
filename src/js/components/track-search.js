import Vue from 'vue/dist/vue.min';
import Axios from 'axios';
import store from '../store/store';

const spotifySettings = {
  tokenUri: '/spotify-token/',
  searchUri: 'https://api.spotify.com/v1/search/',
  resultsLimit: 6
};

const RequestCanceller = Axios.CancelToken;

const cancelMessage = 'updated query was sent.';


export default Vue.component('track-search', {

  props: [ 'recovered-tracks', 'next-el-query' ],

  data () {
    return {
      query: '',
      tracks: [],
      savedTracks: [],
      selectedIndex: 0,
      isFocused: false,
      saving: false,
      nextEl: null,
      requestCanceller: null,
    }
  },

  computed: {
    token() {
      return store.state.spotifyToken;
    },
    results() {
      return this.tracks.map(track => {
        return {
          name: track.name,
          id: track.id,
          album: track.album.name,
          artists: track.artists.map(artist => {
            return artist.name;
          }).join(', ')
        };
      });
    },
    resultsVisible() {
      return this.query.length && this.results.length && this.isFocused;
    },
    placeholder() {
      let placeholder = this.saving ? 'Added!' : 'Add another song...';
      return this.savedTracks.length ? placeholder : 'Add a song to our playlist...';
    }
  },

  methods: {
    search () {
      if (!this.query || !this.query.length) {
        return;
      }

      if (this.requestCanceller) {
        this.requestCanceller.cancel(cancelMessage);
      }
      this.requestCanceller = RequestCanceller.source();

      let params = {
        q: this.query,
        limit: spotifySettings.resultsLimit,
        type: 'track',
        market: 'GB'
      };

      this.currentSearch = Axios.get(spotifySettings.searchUri, {
        cancelToken: this.requestCanceller.token,
        params: params,
        headers: {
          Authorization: `Bearer ${this.token}`
        }
      })
      .then(res => {
        this.tracks = res.data.tracks.items;
      })
      .catch(err => {
        if (err.message === cancelMessage) {
          return;
        }
        console.warn('Spotify search error...', err);
      });
    },

    close() {
      this.tracks = [];
      this.query = '';
      this.selectedIndex = 0;
    },

    focus() {
      this.isFocused = true;

      if (window.innerWidth < 540) {
        document.querySelector('#app-wrapper').scrollTop = 480;
      }
    },

    unfocus() {
      setTimeout(() => {
        this.isFocused = false;
      }, 200);
    },

    keyDown() {
      this.selectedIndex = ++this.selectedIndex % this.tracks.length;
    },

    keyUp() {
      this.selectedIndex = (this.selectedIndex < 1) ? this.tracks.length -1 : --this.selectedIndex;
    },

    keyEnter(e) {
      e.preventDefault();
      e.stopPropagation();
      if (this.tracks[this.selectedIndex]) {
        this.addOrHighlight(this.tracks[this.selectedIndex].id);
        this.close();
      }
    },

    keyTab(e) {
      let shiftDown = (!!window.event && !!window.event.shiftKey) || !!e.shiftKey;
      let nextEl = null;

      if (shiftDown) {
        if (this.resultsVisible) {
          this.keyUp();
          return;
        }
        let checkboxes = document.querySelectorAll('[type="checkbox"]');
        nextEl = checkboxes[checkboxes.length - 2];
      }
      else {
        if (this.resultsVisible) {
          this.keyDown();
          return;
        }
        let savedTracksEl = this.$refs.savedTracks;
        let firstSavedButton = savedTracksEl ? savedTracksEl.querySelector('button') : null;
        nextEl = firstSavedButton || this.nextEl;
      }

      this.$el.blur();

      let focusEvent = new Event('focus');
      nextEl.dispatchEvent(focusEvent);
      nextEl.focus();

    },

    saveTrack(trackId) {
      let added = this.addOrHighlight(trackId);
      this.close();

      if (added) {
        setTimeout(() => {
          this.$refs.input.focus();
          this.focus();
        }, 250);
      }
    },

    addOrHighlight(trackId) {
      if (this.savedTracks.indexOf(trackId) < 0) {
        this.savedTracks.push(trackId);
        this.saving = true;
        setTimeout(() => {
          this.saving = false;
        }, 1000);
        return true;
      }
      else {
        document.querySelector(`#track-${trackId}`).focus();
        return false;
      }
    },

    getToken () {
      Axios.get(spotifySettings.tokenUri)
        .then(res => {
          store.commit('setSpotifyToken', { token: res.data.spotify_token });
        })
        .catch(err => {
          console.warn('Could not get Spotify token...', err);
        });

      // Refresh the token every 30 mins
      if (!this.token) {
        setInterval(this.getToken, 130 * 60 * 1000);
      }
    }
  },

  created () {
    this.getToken();
  },

  mounted () {
    this.savedTracks = this.recoveredTracks ? this.recoveredTracks.split(',') : [];
    this.nextEl = document.querySelector(this.nextElQuery);
  },

  watch: {
    query () {
      this.search();
    },
    resultsVisible () {
      store.commit('setTrackSearchOpen', { isOpen: !!this.resultsVisible });
    }
  }
});
