import Vue from 'vue/dist/vue.min';
import store from '../store/store';


export default Vue.component('footer-template', {
  template: '#footer',

  computed: {
    hidden () {
      return store.state.trackSearchOpen
    }
  }
});
