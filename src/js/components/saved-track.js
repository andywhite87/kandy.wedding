import Vue from 'vue/dist/vue.min';
import Axios from 'axios';
import store from '../store/store';

const spotifySettings = {
  trackUri: 'https://api.spotify.com/v1/tracks/'
};


export default Vue.component('saved-track', {
  props: [ 'track-id' ],

  data() {
    return {
      loading: true,
      name: null,
      album: null,
      artists: null,
      included: true
    }
  },

  computed: {
    token() {
      return store.state.spotifyToken;
    },
    hasToken() {
      return !!this.token;
    }
  },

  methods: {

    getDetails() {
      Axios.get(spotifySettings.trackUri + this.trackId, {
        headers: {
          Authorization: `Bearer ${this.token}`
        }
      })
      .then(res => {
        this.name = res.data.name;
        this.album = res.data.album.name;
        this.artists = res.data.artists.map(artist => {
          return artist.name;
        }).join(', ');
        this.loading = false;
      })
      .catch(err => {
        console.warn('Spotify track info error...', err);
      });
    },

    remove() {
      this.included = false;
    }

  },

  mounted() {
    if (this.hasToken) {
      this.getDetails();
    }

    // Try to get details if token takes a while,
    // as response may be cached
    setTimeout(() => {
      if (!this.hasToken) {
        this.getDetails();
      }
    }, 1000);
  },

  watch: {
    hasToken() {
      if (this.hasToken) {
        this.getDetails();
      }
    }
  }

});
