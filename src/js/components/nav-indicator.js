import Vue from 'vue/dist/vue.min';

import store from '../store/store';


export default Vue.component('nav-indicator', {
  template: `
    <li class="router-nav__indicator"
        aria-hidden="true"
        v-bind:class="{ 'router-nav__indicator-static': !animatable }"
        v-bind:style="{ width: width + 'px', transform: 'translateX(' + x + 'px)' }">
    </li>`,

  data () {
    return {
      resized: true
    }
  },

  computed: {

    animatable () {
      return store.state.appEntered && this.resized;
    },

    activeLink () {
      return document.querySelectorAll('.router-link')[store.state.routeIndex];
    },

    activeLinkText () {
      return this.activeLink.querySelector('span');
    },

    width () {
      if (!store.state.appStarted) {
        return 0;
      }

      let resized = this.resized; // Force recalc after resize
      let offset = (store.state.routeIndex > 2) ? 2: 0;

      return this.activeLinkText.offsetWidth + offset;
    },

    x () {
      if (!store.state.appStarted) {
        return 0;
      }

      let resized = this.resized; // Force recalc after resize
      let offset = (store.state.routeIndex > 2) ? 3 : 5;

      return this.activeLink.offsetLeft + offset;
    }
  },

  created () {
    window.addEventListener('resize', this.handleResize);
  },

  methods: {
    handleResize () {
      this.resized = false;
      window.setTimeout(() => {
        this.resized = true;
      }, 0);
    }
  }
});
