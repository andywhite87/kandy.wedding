import Vue from 'vue/dist/vue.min';
import Axios from 'axios';

import store from '../store/store';
import { isFieldValid, getFieldValues } from '../utils/form-helpers';


const classes = {
  fieldHidden: 'form__hidden',
  formHidden: 'form--faded'
};

const timers = {
  fade: 300
};


export default Vue.component('form-template', {
  template: '#form',

  data () {
    return {
      originalFields: [],
      fields: [],
      guestFields: [],
      attendeeContainers: [],
      nonAttendeeContainers: [],
      nonAttendeeField: null,
      guests: [],
      hasGuests: true,
      error: false,
      submitted: false,
      giftListShown: false
    }
  },

  mounted () {
    if (this.$el.tagName !== 'FORM') {
      return;
    }

    this.fields = this.$el.querySelectorAll('[name]');
    this.guestFields = this.$el.querySelectorAll('[data-guests] [name]');
    this.attendeeContainers = this.$el.querySelectorAll(
      '[data-guests], [data-songs], [data-dietary]');
    this.nonAttendeeContainers = this.$el.querySelectorAll(
      '[data-cannot-attend]');
    this.nonAttendeeField = this.$el.querySelector('#cannot-attend');

    this.values = getFieldValues(this.$el);
    this.originalValues = this.values;

    this.$el.setAttribute('novalidate', true);

    this.addListeners();
    this.updateGuests();
    this.updateFields();
  },

  methods: {

    addListeners () {
      this.$el.addEventListener('submit', this.submit);
      this.$el.addEventListener('change', this.updateFields);
      this.$el.addEventListener('input', this.updateFields);

      this.nonAttendeeField.addEventListener('change', this.resetGuests);

      for (let f = 0; f < this.guestFields.length; f++) {
        this.guestFields[f].addEventListener('change', this.updateGuests);
      }
    },

    updateGuests () {
      this.guests = [];
      for (let f = 0; f < this.guestFields.length; f++) {
        if (this.guestFields[f].checked) {
          this.guests.push(this.guestFields[f].parentElement.textContent.trim());
        }
      }
      this.updateFields();
    },

    updateFields () {

      if (this.hasGuests !== !!this.guests.length) {
        this.$el.classList.add(classes.formHidden);
      }

      this.updateHeight();

      window.setTimeout(() => {
        let attendeeAction = (this.guests.length) ? 'remove' : 'add';
        let nonAttendeeAction = (this.guests.length) ? 'add' : 'remove';

        for (let ac = 0; ac < this.attendeeContainers.length; ac++) {
          this.attendeeContainers[ac].classList[attendeeAction](classes.fieldHidden);
        }

        for (let nac = 0; nac < this.nonAttendeeContainers.length; nac++) {
          this.nonAttendeeContainers[nac].classList[nonAttendeeAction](classes.fieldHidden);
        }

        if (!this.guests.length) {
          this.nonAttendeeField.checked = true;
          this.nonAttendeeField.focus();
        }

        this.hasGuests = !!this.guests.length;

        store.commit('setFormDirty', { isDirty: this.isDirty() });

        this.$el.classList.remove(classes.formHidden);
      }, timers.fade);
    },

    updateHeight () {
      window.setTimeout(() => {
        this.$el.removeAttribute('style');
        this.$el.setAttribute('style', this.hasGuests ?
          `min-height: ${this.$el.clientHeight}px` :
          'auto');
      }, timers.fade);
    },

    resetGuests () {
      for (let f = 0; f < this.guestFields.length; f++) {
        if (!this.guestFields[f].checked) {
          this.guestFields[f].click();
        }
      }
      setTimeout(() => {
        this.guestFields[0].focus();
      }, timers.fade);
      this.updateFields();
    },

    isDirty () {
      this.values = getFieldValues(this.$el);

      let isDirty = false;
      for (let key in this.values) {
        if (this.values[key] !== this.originalValues[key]) {
          isDirty = true;
        }
      }

      if (isDirty) {
        window.onbeforeunload = function(e) {
          let dialogText = store.state.formDirtyText;
          e.returnValue = dialogText;
          return dialogText;
        };
      }

      else {
        window.onbeforeunload = null;
      }

      return isDirty;
    },

    isValid () {
      let isValid = true;
      for (let f = 0; f < this.fields.length; f++) {
        if (!isFieldValid(this.fields[f])) {
          isValid = false;
          break;
        }
      }
      return isValid;
    },

    createFormData () {
      let fields = [];

      let codeField = this.$el.querySelector('[name="guest-code"]');
      let commentField = this.$el.querySelector('[name="comment"]');
      let dietaryField = this.$el.querySelector('[name="dietary-requirements"]');
      let attendingFields = this.$el.querySelectorAll('[name="guest-group"]');
      let songFields = this.$el.querySelectorAll('[name="song-id"]');

      for (let f = 0; f < attendingFields.length; f++) {
        if (attendingFields[f].checked) {
          fields.push(`${attendingFields[f].name}=${encodeURIComponent(attendingFields[f].value)}`);
        }
      }
      for (let f = 0; f < songFields.length; f++) {
        fields.push(`${songFields[f].name}=${encodeURIComponent(songFields[f].value)}`);
      }

      fields.push(`${commentField.name}=${encodeURIComponent(commentField.value)}`);
      fields.push(`${dietaryField.name}=${encodeURIComponent(dietaryField.value)}`);

      fields.push(`${codeField.name}=${encodeURIComponent(codeField.value)}`);

      return '?' + fields.join('&');
    },

    submit (e) {
      e.preventDefault();

      // let data = [];
      // for(let f = 0; f < this.fields.length; f++) {
      //    data.push(this.fields[f]['name'] + '=' + this.fields[f].value);
      // }

      let dataString = this.createFormData();

      Axios.post(
        this.$el.getAttribute('action') + dataString)
        .then(() => { this.onSuccess(); })
        .catch((e) => { this.onError(e); });
    },

    reload () {
      store.commit('setFormDirty', { isDirty: false });
      window.onbeforeunload = null;
      setTimeout(() => {
        window.location.reload();
      }, 0);
    },

    onError (e) {
      this.error = true;
    },

    onSuccess () {
      this.submitted = true;
      setTimeout(() => {
        this.$refs.message.focus();
      }, timers.fade);
    }
  },

  watch: {
    giftListShown() {
      setTimeout(() => {
        if (this.giftListShown) {
          this.$refs.giftListMessage.focus();
        }
        else {
          this.$refs.message.focus();
        }
      }, timers.fade);
    }
  }

});
