import Vue from 'vue/dist/vue.min';
import Vuex from 'vuex';
Vue.use(Vuex);

import { routePaths } from '../router/router';


export default new Vuex.Store({
  state: {
    appStarted: false,
    appEntered: false,
    routeIndex: 0,
    routePaths: routePaths,
    formDirty: false,
    formDirtyText: `You haven't sent your RSVP yet. ` +
                   `Are you sure you want to leave?`,
    spotifyToken: null,
    trackSearchOpen: false
  },
  mutations: {
    appStart(state) {
      state.appStarted = true;
    },
    appEnterComplete(state) {
      state.appEntered = true;
    },
    routeUpdate(state, payload) {
      let index = state.routePaths.indexOf(payload.path);
      state.routeIndex = index < 0 ? 0 : index;
    },
    setRoutePaths(state, payload) {
      state.routePaths = payload.routePaths;
    },
    setFormDirty(state, payload) {
      state.formDirty = payload.isDirty;
    },
    setSpotifyToken(state, payload) {
      state.spotifyToken = payload.token;
    },
    setTrackSearchOpen(state, payload) {
      state.trackSearchOpen = payload.isOpen;
    }
  }
});
