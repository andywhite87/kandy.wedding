import Vue from 'vue/dist/vue.min';
import mustardTest from './utils/mustard-test'

import { router, routeLinks, routePaths, routeTemplate } from './router/router';
import store from './store/store';
import { saveCode, loadCode } from './utils/guest-helpers';
import FooterTemplate from './components/footer';
import FormTemplate from './components/form';
import SavedTrack from './components/saved-track';
import TrackSearch from './components/track-search';


(function() {

  // Firefox test
  if ('netscape' in window) {
    mustardTest();
    return;
  }

  // IE test
  if (navigator.appName == 'Microsoft Internet Explorer' ||
      !!(navigator.userAgent.match(/Trident/) ||
      navigator.userAgent.match(/rv:11/))) {
    document.body.classList.add('ie');
  }

  // Bootstrap the Vuex store
  store.commit('setRoutePaths', { routePaths: routePaths });
  store.commit('routeUpdate', { path: window.location.pathname });

  // Set the app to start after short anim delays
  window.setTimeout(() => {
    store.commit('appStart', {});
  }, 2000);
  window.setTimeout(() => {
    store.commit('appEnterComplete', {});
  }, 3000);

  // Remove no-JS marker for CSS
  document.body.removeAttribute('nojs');
  document.body.setAttribute('js', '');


  // Initialise the app with router and nested views
  new Vue({
    router,
    data () {
      return {
        store: store
      };
    },
    components: {
      'route-template': routeTemplate,
      'form-template': FormTemplate,
      'footer-template': FooterTemplate,
      'saved-track': SavedTrack,
      'track-search': TrackSearch
    },
    template: `
      <div id="app"
           :data-entered="store.state.appEntered">
        <route-template></route-template>
        <footer-template></footer-template>
      </div>
    `,
    mounted () {
      let dir = window.location.pathname;
      let guestCode = document.querySelector('body').getAttribute('guest-code');
      if (dir.length > 1 && routePaths.indexOf(dir) < 0) {
        saveCode(guestCode);
      }
      else {
        loadCode();
      }
    }
  }).$mount('#app');

})();
