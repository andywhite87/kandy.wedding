export default function mustardTest() {

  const noscripts = document.querySelectorAll('noscript');
  const sections = document.querySelectorAll('.section');
  const contentSection = sections[sections.length - 1];
  const contentSectionInner = contentSection.querySelector('.section__inner');
  let htmlData = [];

  for (let n = 0; n < noscripts.length; n++) {
    htmlData.push(noscripts[n].innerHTML);
  }

  contentSection.classList.add('child-view');
  for (var d = 0; d < htmlData.length; d++) {
    contentSectionInner.innerHTML += htmlData[d];
  }

  const newSections = document.querySelectorAll('.section');
  const lastSection = newSections[newSections.length - 1];
  const lastSectionInner = lastSection.querySelector('.section__inner');
  const formData = lastSection.querySelector('noscript');
  lastSectionInner.innerHTML += formData.innerHTML;

};
