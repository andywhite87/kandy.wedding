const patternMatches = function(fieldEl, value) {
  let pattern = fieldEl.getAttribute('pattern');
  let regPattern;

  if (!pattern || !pattern.length) {
    return true;
  }

  regPattern = new RegExp(pattern);
  return regPattern.test(value);
};


const isFieldValid = function(fieldEl) {
  let isRequired = fieldEl.hasAttribute('required');
  let value = getFieldValue(fieldEl);

  // Allow empty fields if not required
  if (!isRequired && !value) {
    return true;
  }

  // Disallow fields with mismatched values even if not required
  else if (!isRequired && patternMatches(fieldEl, value)) {
    return true;
  }

  // Allow required fields which match pattern
  else if (isRequired && value && patternMatches(fieldEl, value)) {
    return true;
  }

  // Disallow any other state
  return false;
};


const getFieldValue = function(fieldEl) {
  return (fieldEl.getAttribute('type') === 'checkbox') ?
      fieldEl.checked : fieldEl.value;
}


const getFieldValues = function(formEl) {
  let values = {};
  let fields = formEl.querySelectorAll('[name]')

  for (let f = 0; f < fields.length; f++) {
    values[fields[f].getAttribute('name')] = getFieldValue(fields[f]);
  }

  return values;
};


export { isFieldValid, getFieldValues };
