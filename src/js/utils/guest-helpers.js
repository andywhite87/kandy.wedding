import { router } from '../router/router';


const codeKey = 'guest-code';
const nextKey = 'next-dest';


function saveCode(code) {
  if (!code) {
    return;
  }

  window.localStorage.setItem(codeKey, code);
  router.push('/');
}

function loadCode() {
  let code = window.localStorage.getItem(codeKey);

  if (code) {
    window.localStorage.setItem(nextKey, window.location.pathname);
    window.location.pathname = '/' + code;
  }
}


export { saveCode, loadCode };
