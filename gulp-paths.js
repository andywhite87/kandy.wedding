module.exports = PATHS = {
  DIST: {
    IMG: 'dist/img',
    JS: 'dist/js',
    SCSS: 'dist/css',
    SW: 'dist/sw'
  },
  SRC: {
    IMG: 'src/img/**/*',
    JS: 'src/js/**/*.js',
    SCSS: 'src/scss/**/*.scss',
    SW: 'src/js/sw.js'
  }
};
