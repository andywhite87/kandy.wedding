var autoprefixer = require('gulp-autoprefixer');
var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var envify = require('envify/custom');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var rename = require('gulp-rename');
var scss = require('gulp-sass');
var scssLint = require('gulp-scss-lint');
var shell = require('gulp-shell');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

var PATHS = require('./gulp-paths');
var PROJECT = 'kandy';

var isProd = function() {
  return process.env.NODE_ENV === 'production';
};

/**
 * Core functions
 * Compile, minify and copy CSS, JS and images
 */

gulp.task('js', function() {
  gulp.src([PATHS.DIST.JS + '**/*'])
      .pipe(clean({ force: true }));

  browserify({
    entries: PATHS.SRC.JS.split('*')[0] + '/main.js',
    debug: !isProd() })
    .transform('babelify', {
      presets: ['es2015'],
      sourceMaps: !isProd() })
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(gulpif(!isProd(), sourcemaps.init({ loadMaps: true })))
    .pipe(gulpif(isProd(), uglify()))
    .pipe(rename(PROJECT + '.min.js'))
    .pipe(gulpif(!isProd(), sourcemaps.write('./')))
    .pipe(gulp.dest(PATHS.DIST.JS));
});


gulp.task('sw', function() {
  gulp.src(PATHS.SRC.SW)
      .pipe(gulp.dest(PATHS.DIST.SW));
});


gulp.task('scss', function() {
  gulp.src([PATHS.DIST.SCSS + '**/*'])
  .pipe(clean({force: true}));

  gulp.src(PATHS.SRC.SCSS)
      .pipe(gulpif(!isProd(), sourcemaps.init({ loadMaps: true })))
      .pipe(scssLint({config: '.scss-lint.yml'}))
      .pipe(scss().on('error', scss.logError))
      .pipe(scss({outputStyle: isProd() ? 'compressed' : 'expanded'}))
      .pipe(autoprefixer({
        browsers: ['last 4 versions'],
        cascade: false
      }))
      .pipe(rename(PROJECT + '.min.css'))
      .pipe(gulpif(!isProd(), sourcemaps.write('./')))
      .pipe(gulp.dest(PATHS.DIST.SCSS));
});


gulp.task('images', function() {
  gulp.src([PATHS.DIST.IMG + '**/*'])
  .pipe(clean({force: true}));

  gulp.src(PATHS.SRC.IMG)
      .pipe(imagemin([
         imagemin.jpegtran({progressive: true}),
         imagemin.optipng({optimizationLevel: 7})
      ]))
      .pipe(gulp.dest(PATHS.DIST.IMG));
});


/**
 * Gulp tasks
 * Shortcuts for compiling and running builds
 */

gulp.task('dev', ['js', 'sw', 'scss', 'images']);


gulp.task('watch', ['dev'],
    function() {
      gulp.watch(PATHS.SRC.IMAGES, ['images']);
      gulp.watch([PATHS.SRC.JS, PATHS.SRC.JS], ['js']);
      gulp.watch([PATHS.SRC.SW, PATHS.SRC.SW], ['sw']);
      gulp.watch(PATHS.SRC.SCSS, ['scss']);
      console.log('Dev build successful. Watching files...');
    });

gulp.task('serve', ['dev'], shell.task([
  'dev_appserver.py ./'
]));

gulp.task('build', shell.task([
  'NODE_ENV=production gulp dev'
]));


gulp.task('deploy', ['build'], shell.task([
  '~/gae/google_appengine/appcfg.py update . --no_cookies'
]));


gulp.task('default', ['watch']);
