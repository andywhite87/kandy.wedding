# Kandy Wedding

## Libraries and technologies

- Originally based on the App Engine [Guestbook sample app](https://cloud.google.com/appengine/docs/python/getting-started/creating-guestbook)
- Backend written in [Python](https://python.org) 2.7 using [webapp2](http://webapp-improved.appspot.com/) and [jinja2](http://jinja.pocoo.org/docs/)
- Runs on [App Engine](https://developers.google.com/appengine)
- Data stored in the Datastore via the [NDB Datastore API](https://developers.google.com/appengine/docs/python/ndb/)

## Development

To start the dev server:

`dev_appserver.py ./`

In a separate terminal window, run linters and preprocessors:

```
gulp
```

## Deploying

Make sure you have successfully run the development server and built the site.

After bumping the version number in `app.yaml` and commiting all changes, run:

```
~/gae/google_appengine/appcfg.py update .
```

Where `~/gae/google_appengine` is the path to the App Engine SDK on your machine.

Verify the changes and then migrate traffic to the new version in the Google Cloud Platform console.
