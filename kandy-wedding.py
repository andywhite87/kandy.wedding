import base64
import json
import jinja2
import webapp2
import os
import re
import urllib
import urlparse

from google.appengine.api import users
from google.appengine.api import urlfetch
from google.appengine.ext import ndb

from kandy.rsvp import Rsvp
from kandy.guests import lookup_guests, all_guests, guest_count, filter_out_guests


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

RSVP_KEY = 'kandy_rsvp'

SPOTIFY_AUTH_URI = 'https://accounts.spotify.com/api/token'
SPOTIFY_CLIENT_ID = 'c2cd0ff4d7b3441bbd72923fe32e8927'
SPOTIFY_CLIENT_SECRET = '1e6e6257b0b544ffb3b916ded4e22c76'

def rsvp_key():
    return ndb.Key('RSVP', RSVP_KEY)

def past_rsvp(guest_code):
    past_rsvp_answers = None
    rsvp_query = Rsvp.query(ancestor=rsvp_key())
    past_rsvp = rsvp_query.filter(Rsvp.guest_code == guest_code).get()
    if past_rsvp:
        past_rsvp_answers = {
            'guests': past_rsvp.guest_group,
            'song': past_rsvp.song,
            'song_ids': past_rsvp.song_ids,
            'dietary_requirements': past_rsvp.dietary_requirements,
            'comment': past_rsvp.comment
        }
    return past_rsvp_answers


class MainPage(webapp2.RequestHandler):

    def get(self):
        path = urlparse.urlparse(self.request.url).path
        guest_code = path.split('/')[1].lower()
        guests = lookup_guests(guest_code)
        past_rsvp_answers = past_rsvp(guest_code)

        template_values = {}
        if guests:
            template_values = {
                'guests': guests['guests'],
                'has_kids': 'kids' in guests and guests['kids'],
                'guest_code': guest_code,
                'past_rsvp': past_rsvp_answers
            }

        template = JINJA_ENVIRONMENT.get_template('templates/public/index.html')

        self.response.write(template.render(template_values))


class AdminPage(webapp2.RequestHandler):

    def get(self):
        rsvp_query = Rsvp.query(ancestor=rsvp_key())

        rsvps_total = all_guests()

        rsvps_accepted = rsvp_query.filter(Rsvp.can_attend == True).order(-Rsvp.rsvp_date).fetch()
        rsvps_declined = rsvp_query.filter(Rsvp.can_attend == False).order(-Rsvp.rsvp_date).fetch()
        rsvps_not_responded = filter_out_guests(all_guests(), [ rsvps_accepted, rsvps_declined ])

        rsvps_accepted_count = 0
        rsvps_declined_count = 0
        rsvps_not_responded_count = 0

        for rsvp in rsvps_accepted:
            rsvps_accepted_count += rsvp.guest_count

        for rsvp in rsvps_declined:
            rsvps_declined_count += rsvp.not_guest_count

        for rsvp in rsvps_not_responded:
            rsvps_not_responded_count += len(lookup_guests(rsvp)['guests'])

        user = users.get_current_user()

        template_values = {
            'user': user,
            'rsvps_accepted': rsvps_accepted,
            'rsvps_declined': rsvps_declined,
            'rsvps_not_responded': rsvps_not_responded,
            'rsvps_accepted_count': rsvps_accepted_count,
            'rsvps_declined_count': rsvps_declined_count,
            'rsvps_not_responded_count': rsvps_not_responded_count,
            'simple_template': True,
            'admin': True
        }

        template = JINJA_ENVIRONMENT.get_template('templates/admin/index.html')
        self.response.write(template.render(template_values))


class Get_Spotify_Token(webapp2.RequestHandler):

    def get(self):
        token = None

        try:
            auth_code = base64.b64encode(bytes('{0}:{1}'.format(SPOTIFY_CLIENT_ID, SPOTIFY_CLIENT_SECRET)))
            payload = urllib.urlencode({ 'grant_type': 'client_credentials' })
            headers = { 'Authorization': 'Basic {0}'.format(auth_code) }
            spotify_data = urlfetch.fetch(
                url=SPOTIFY_AUTH_URI,
                payload=payload,
                method=urlfetch.POST,
                headers=headers)
            token = json.loads(spotify_data.content)['access_token']
        except urlfetch.Error:
            token = json.loads('null')


        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(json.dumps({ 'spotify_token': token }))


class Send_Rsvp(webapp2.RequestHandler):

    def post(self):
        # We set the same parent key on the 'Greeting' to ensure each
        # Greeting is in the same entity group. Queries across the
        # single entity group will be consistent. However, the write
        # rate to a single entity group should be limited to
        # ~1/second.

        # Get the guest code from the post
        try:
            guest_code = self.request.get('guest-code').lower()
            allowed_guests = lookup_guests(guest_code)['guests']
            past_rsvp_answers = past_rsvp(guest_code)

            # TODO create error
            if not allowed_guests or len(allowed_guests) < 1:
               self.redirect('/')
               return

            # Grab the existing entry with this code, or create a new one
            rsvp = Rsvp.query(Rsvp.guest_code == guest_code).get()
            if not rsvp:
                rsvp = Rsvp(parent=rsvp_key())

            # Set the entry's data
            rsvp.guest_code = guest_code

            rsvp.guest_group = self.request.get_all('guest-group')
            rsvp.guest_count = len(rsvp.guest_group)
            rsvp.can_attend = rsvp.guest_count > 0
            rsvp.not_guest_group = []
            for g in allowed_guests:
              if g not in rsvp.guest_group:
                rsvp.not_guest_group.append(g)
            rsvp.not_guest_count = len(rsvp.not_guest_group)

            rsvp.dietary_requirements = self.request.get('dietary-requirements')
            rsvp.comment = self.request.get('comment')

            song_ids = self.request.get_all('song-id')
            song = self.request.get('song')
            if len(song_ids):
                rsvp.song_ids = song_ids
                rsvp.song = past_rsvp_answers['song'] if past_rsvp_answers else ''
            elif song:
                rsvp.song = song
                rsvp.song_ids = past_rsvp_answers['song_ids'] if past_rsvp_answers else []

            rsvp.put()

            if guest_code:
                self.redirect('/success/{0}'.format(guest_code))
                return
            else:
               self.redirect('/')
               return

        except:
            self.redirect('/error{0}'.format(guest_code))
            return


class Rsvp_Success(webapp2.RequestHandler):

    def get(self, guest_code):

        guest_code = re.sub(r'\W+', '', guest_code)
        guests = lookup_guests(guest_code)['guests']
        rsvp_answers = past_rsvp(guest_code)

        if  guests and rsvp_answers:
            template = JINJA_ENVIRONMENT.get_template('templates/public/success.html')
            template_values = {
                'guests_coming': len(rsvp_answers['guests']),
                'guests_not_coming': len(guests) - len(rsvp_answers['guests']),
                'guest_code': guest_code,
                'simple_template': True
            }
            self.response.write(template.render(template_values))
            return
        else:
           self.redirect('/{0}'.format(guest_code))
           return


class Rsvp_Error(webapp2.RequestHandler):

    def get(self, guest_code):

        guest_code = re.sub(r'\W+', '', guest_code)

        template = JINJA_ENVIRONMENT.get_template('templates/public/error.html')
        template_values = {
            'guest_code': guest_code,
            'simple_template': True
        }
        self.response.write(template.render(template_values))
        return


class Rsvp_Gift_List(webapp2.RequestHandler):

    def get(self):
        template = JINJA_ENVIRONMENT.get_template('templates/public/gift-list.html')
        template_values = {
            'simple_template': True
        }
        self.response.write(template.render(template_values))
        return


app = webapp2.WSGIApplication([
    (r'/admin/?$', AdminPage),
    (r'/spotify-token/?$', Get_Spotify_Token),
    (r'/send-rsvp/?$', Send_Rsvp),
    (r'/success(.*)', Rsvp_Success),
    (r'/error(.*)', Rsvp_Error),
    (r'/gift-list/?$', Rsvp_Gift_List),
    (r'/.*', MainPage),
# ], debug=True)
])
